// ПЕРЕМЕННЫЕ
// var message; // a-z, A-Z, 0-9, $, _
// message = "MyMessage";
//
// var myNumber = 10;
// var myNull = null;
// var myBoolean = true;
// var myUndefined = undefined;
// var myObject = {
//     name: "Вася",
//     age: 25
// };
//
// var mySecondObject = {
//     name: "Петя",
//     age: 41
// };

// ЧИСЛА
// var num = 10 + 40;
// num *= 10;
// console.log(num);
// console.log(Math.round(5.50));
// console.log(10 + 10);
// console.log(myNull);
// console.log(myBoolean);
// console.log(myUndefined);
// console.log(myObject);
// console.log(mySecondObject);
//
// var newNum = 2.34534;
// console.log(newNum.toFixed(2));

// СТРОКИ
// console.log("40" + myNumber);
// console.log(message.toLowerCase());

// МАССИВЫ
// var names = ["Вася", "Петя", "Женя"];
// console.log(names[1]);
// console.log(names[0].toLocaleUpperCase());
//
// names[0] = "Masha";
// console.log(names[0].toLocaleUpperCase());
//
// names.push("Оля")
// console.log(names);

// УСЛОВИЯ
// var numeric = 123;
// var string = "123";
//
// if (numeric !== string) {
//     console.log("true")
// }

//ЦИКЛЫ
// for (var i = 0; i < 10; i++) {
//     if (i == 5) break;
//     console.log(i);
// }
//
// for (var r = 0; r < names.length; r++) {
//     console.log(names[r]);
// }

// let m = 0;
// var t;
// while (t < 10) {
//     let t = m;
//     console.log(t);
//     t++;
// }

// let i = 0;
// while (i < 3) {
//     console.log(i);
//     i++;
// }

// ФУНКЦИИ
// var a, b;
// function sum(a, b) {
//     return a + b;
// }
//
// console.log(sum(10, 17));


// ОБЪЕКТЫ
// declare class Main{
//     name: 'ewer';
// }
// const qwer = Main;
// console.log(qwer.name);

var myObject = {
    name: "Vasily",
    age: 25
};

var youObject = { name: "Anton", age: 27 };

console.log(myObject.age);