class Student {
    constructor(name) {
        this.name = name
    }

    say() {
        console.log(`Hi, my name is ${this.name}`)
    }
}

class ProtoStudent {
    university = 'Oxford'
}

const student = Reflect.construct(Student, ['Igor'])
// console.log(student.__proto__ === ProtoStudent.prototype)

Reflect.apply(student.say, {name: 'Tester'}, [])
console.log(Reflect.ownKeys(student))

Reflect.preventExtensions(student) //заблокировал модификацию объекта 'student'
student.age = 25

console.log(Reflect.isExtensible(student))
console.log(student)