class Person {
    type = 'human'
    constructor(name) {
        this.name = name
    }
    say() {
        console.log(`${this.name} говорит привет!`)
    }
}

class Programmer extends Person {
    constructor(name, job) {
        super(name)
        this._job = job
    }

    get job() {
        return this._job.toUpperCase()
    }

    set job(job) {
        if (job.length < 2) console.log('Validation failed')
        else this._job = job
    }

    say() {
        super.say()
        console.log('dfgdsfghdfs')
    }
}

const frontend = new Programmer("Tolik", 'Frontend')
// console.log(frontend.job)
frontend.job = 'Backend'
// console.log(frontend.job)

// STATIC
class Util {
    static average(...args) {
        return args.reduce((acc, i) => acc += i, 0) / args.length
    }

    average2(...args) {
        return args.reduce((acc, i) => acc += i, 0) / args.length
    }
}

// const util = new Util()
// console.log(util.average(1, 1, 2, 3, 5, 8, 13, 21))

console.log(Util.average(1, 1, 2, 3, 5, 8))
console.log(new Util().average2(1, 1, 2, 3, 5, 8, 13))