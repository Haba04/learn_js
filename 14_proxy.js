const validator = {
    get(target, prop) {
        return prop in target ? target[prop] : `Поля ${prop} в объекте нет.`
    },

    set(target, prop, value) {
        if (value.length > 2) {
            Reflect.set(target, prop, value)
        } else {
            console.log(`Длина должна быть больше 2-х символов`)
        }
    }
}